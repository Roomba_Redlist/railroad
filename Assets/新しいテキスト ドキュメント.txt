#include <stdio.h>

int main() {
  int num;
  printf("数値を入力してください：");
  scanf("%d", &num);

  if (num > 0) {
    printf("入力された数値は正の数です。\n");
  } else if (num == 0) {
    printf("入力された数値は0です。\n");
  } else {
    printf("入力された数値は負の数です。\n");
  }

  return 0;
}
