﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UndoButton : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnPushedUndoButton()
    {
        int countInHistory = 0;
        int historySize = transform.parent.childCount;
        for (int i=0; i < historySize; i++)
        {
            if (transform.parent.GetChild(i).gameObject == this.gameObject)
            {
                countInHistory = i;
                break;
            }
        }
        Debug.Log("このボタンの番号： "+countInHistory);
        GameObject.Find("UndoHandler").GetComponent<UndoHandler>().StartCoroutine("UndoMultiple",historySize - countInHistory);
    }
}
