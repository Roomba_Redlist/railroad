﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using RailroadSystem;

public class UndoHandler : MonoBehaviour
{
    public GameObject UndoHistory, UndoButton;
    [SerializeField]
    BallBase ball;
    public bool isAbleUndo=true;
    public List<UndoPoint> undoPoint = new List<UndoPoint>();
    SpeedManager speedManager;
    //UndoButtonにRPの情報を持たせて、一気に遡れるようにしたいな（操作履歴からUndoみたいな）
    //余裕あったら時間停止バッファつけたい
    //bool isTimeStopPressed;
    // Start is called before the first frame update

    public class UndoPoint
    {
        public Vector3 position;
        public List<string> exeLog;
        public ReturnPoint removedRP;
        public string movedFunc;

        public UndoPoint(Vector3 pos, List<string> st, ReturnPoint rp, string func)
        {
            position = pos;
            exeLog = st;
            removedRP = rp;
            movedFunc = func;
        }
    }
    public UndoPoint GetCurrentUP()
    {
        if (undoPoint.Count == 0)
        {
            Debug.LogError("UPが残っていない");
            return undoPoint[undoPoint.Count];
        }
        return undoPoint[undoPoint.Count - 1];
    }

    public void AddUP(List<string> str)
    {
        undoPoint.Add(new UndoPoint(ball.transform.position, str, null, null));
        AddHistory();
    }
    public void AddUP(List<string> str,string func)
    {
        undoPoint.Add(new UndoPoint(ball.transform.position, str, null, func));
        AddHistory();
    }

    public void AddUP(List<string> str,ReturnPoint rp, string func)
    {
        undoPoint.Add(new UndoPoint(ball.transform.position, str, rp, func));
        AddHistory();
    }

    public void RemoveUP()
    {
        undoPoint.RemoveAt(undoPoint.Count - 1);
    }

    public void UndoBall(BallBase ball)
    {
        if (undoPoint.Count != 0)
        {
            UndoPoint _up = GetCurrentUP();
            ball.PrevLine();
            if (_up.removedRP != null)
            {
                ball.returnPoint.Add(_up.removedRP);
            }
            if (_up.movedFunc != null)
            {
                ball.currentFunc = _up.movedFunc;
            }
            ball.transform.position = _up.position;
            RemoveUP();
        }
        else Debug.LogError("これ以上Undoできない");
    }

    string HistoryText(UndoPoint up)
    {
        List<string> _str = up.exeLog;
        string result="test";

        switch (_str[0])
        {
            case "func_start":
                result = up.movedFunc + "から"+_str[1] + "へcall";
                break;
            case "func_end":
                result = _str[1] + "から"+ up.movedFunc + "へreturn";
                break;
            case "then":
                result = "ID" + _str[1] + "の分岐で真";
                break;
            case "else":
                result = "ID" + _str[1] + "の分岐で偽";
                break;
            case "bloop_start":
                result = "ID"+_str[1]+"の先判定ループの先頭";
                break;
            case "bloop_bottom":
                result = "ID" + _str[1] + "の先判定ループ終了";
                break;
        }

        return ball.lindex+":"+result;
    }

    public void AddHistory()
    {
        GameObject buttonInst;
        buttonInst=Instantiate(UndoButton, UndoHistory.transform.position, Quaternion.identity, UndoHistory.transform);
        buttonInst.transform.GetChild(0).GetComponent<Text>().text = string.Join(", ", HistoryText(GetCurrentUP()));
    }

    public void RemoveHistory()
    {
        Destroy(UndoHistory.transform.GetChild(UndoHistory.transform.childCount - 1).gameObject);
    }
    void Start()
    {
        speedManager = GameObject.Find("SpeedManager").GetComponent<SpeedManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("left"))
        {
                if (undoPoint.Count > 0)
                {
                    StartCoroutine(Undo());
                }
                else
                {
                    Debug.LogError("UPが残っていないためUndoできない");
                }

        }
    }

    public IEnumerator UndoMultiple(int count)
    {
        if (isAbleUndo)
        {
            isAbleUndo = false;
            Debug.Log("Undoしたため、待機をキャンセルします");
            Debug.Log("-------------------------------------");
            Debug.Log(count + "回Undoします");
            speedManager.SetTimeStop(false);
            speedManager.isAbleTimeStop = false;
            ball.SetPhysicsEnabled(false);

            for (int i = 0; i < count; i++)
            {
                RemoveHistory();
                UndoBall(ball);
                yield return new WaitForSeconds(0.05f);
            }

            yield return new WaitForSeconds(0.1f);
            speedManager.isAbleTimeStop = true;
            speedManager.SetTimeStop(true);
            yield return null;
            ball.SetPhysicsEnabled(true);
            //ボールの当たり判定と物理を消し、カメラが動き終わったら時間を止める
            isAbleUndo = true;
        }
        else Debug.LogWarning("今はundoできない");
    }

    public IEnumerator Undo()
    {
        if (isAbleUndo)
        {
            isAbleUndo = false;
            Debug.Log("Undoしたため、待機をキャンセルします");
            Debug.Log("-------------------------------------");
            RemoveHistory();
            speedManager.SetTimeStop(false);
            speedManager.isAbleTimeStop = false;
            ball.SetPhysicsEnabled(false);
            UndoBall(ball);
            yield return new WaitForSeconds(0.2f);

            speedManager.isAbleTimeStop = true;
            speedManager.SetTimeStop(true);
            yield return null;
            ball.SetPhysicsEnabled(true);
            //ボールの当たり判定と物理を消し、カメラが動き終わったら時間を止める
            isAbleUndo = true;
        }
        else Debug.LogWarning("今はundoできない");
    }
}
