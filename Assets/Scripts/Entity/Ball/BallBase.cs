﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using RailroadSystem;

public class BallBase : MonoBehaviour
{
    public List<List<string>> exeLog;
    List<string> line;
    string nextRail;
    bool isTrueStatement;
    public int lindex=0;
    public string currentFunc = "main";
    public string nextFunc = "-1";
    public List<ReturnPoint> returnPoint = new List<ReturnPoint>();
    public GameObject _markerobject;
    public UndoHandler undoHandler;
    // Start is called before the first frame update
    public void Activate(List<List<string>> log)
    {
        exeLog = log;
        lindex = 0;
        //ShapeLog();
        CheckLog();
        //PutAllExeLog();
    }
    void PutAllExeLog()
    {
        Debug.Log(exeLog.Count);
        for (int i=0;i<exeLog.Count-1;i++)
        {
            Debug.Log(exeLog[i][0] + "," + exeLog[i][1]);
        }
    }
    public void SetPhysicsEnabled(bool _bool)
    {
        GetComponent<Rigidbody>().isKinematic = !_bool;
        GetComponent<Collider>().enabled = _bool;
    }
    //public void ShapeLog()
    //{
    //    for (int i = 0; i < exeLog.Count; i++)
    //    {
    //        switch ((exeLog[i])[0])
    //        {
    //            case "then":
    //                break;

    //            case "else":
    //                break;

    //            case "aloop_end":
    //                break;

    //            case "bloop_start":
    //                break;

    //            case "bloop_bottom":
    //                break;

    //            case "func_start":
    //                break;

    //            case "func_end":
    //                break;

    //            default:
    //                exeLog.RemoveAt(i);
    //                break;


    //        }

    //    }
    //}

    public void ExecuteRail(RailBase rail){
        Debug.Log("レール動作を実行し、次の目的地を設定します");
        rail.ExecuteRailAction(isTrueStatement);
        Debug.Log("-------------------------------------");
        NextLine();
    }
    public void AddRP()
    {
        returnPoint.Add(new ReturnPoint(this.transform.position,currentFunc));
        //returnPoint[returnPoint.Count - 1].rpTransform.localPosition += new Vector3(0,0,0.5f);
        //Instantiate(
        //    _markerobject,
        //    returnPoint[returnPoint.Count-1].position,
        //    Quaternion.identity
        //    );
        Debug.Log("Ballの帰還地点が生成された");
        PutAllRP();
    }
    public void PutAllRP()
    {
        Debug.Log("現在のRP：");
        if (returnPoint.Count == 0) Debug.LogError("RPが残っていない");
        foreach (ReturnPoint rp in returnPoint)
        {
            Debug.Log("┗" + rp.position + " , " + rp.rpName);
        }
    }
    public void RemoveRP()
    {
        if (returnPoint.Count==0) Debug.LogError("RPが残っていない");
        returnPoint.RemoveAt(returnPoint.Count - 1);
        PutAllRP();
    }
    public ReturnPoint GetCurrentRP()
    {
        if (returnPoint.Count==0) Debug.LogError("RPが残っていない");
        return returnPoint[returnPoint.Count - 1];
    }
    public void UpdateCurrentFunc(string str)
    {
        currentFunc = str;
        Debug.Log("現在の関数＝" + currentFunc);
    }

    public void NextLine()
    {
        lindex++;
        CheckLog();
    }
    public void PrevLine()
    {
        lindex--;
        CheckLog();
    }

    public void CheckLog()
    {
        switch ((exeLog[lindex])[0])
        {
            case "then":
                nextRail = "TwoWay";
                isTrueStatement = true;
                break;

            case "else":
                nextRail = "TwoWay";
                isTrueStatement = false;
                break;

            case "aloop_end":
                nextRail = "ALoopEnd";
                isTrueStatement = true;
                if ((exeLog[lindex + 1])[0] == "aloop_bottom")
                {
                    isTrueStatement = false;
                }
                break;

            case "bloop_start":
                nextRail = "BLoopStart";
                isTrueStatement = true;
                break;

            case "bloop_bottom":
                nextRail = "BLoopStart";
                isTrueStatement = false;
                break;

            case "func_start":
                //Debug.LogError(exeLog[lindex][1]);
                if (exeLog[lindex][1] == "main")
                {
                    Debug.LogError("main skipped");
                    NextLine();
                    break;
                }
                nextRail = "Gate";
                isTrueStatement = true;
                break;

            case "func_end":
                nextRail = "Return";
                isTrueStatement = true;
                break;


            default:
                NextLine();
                break;

        }
        Debug.Log("現在の目的地…行数:" + lindex + ",タイプ:" + (exeLog[lindex])[0] + ",ID:" + (exeLog[lindex])[1] + ",真偽:" + isTrueStatement);
    }

    public void CheckRail(RailBase rail)
    {
        Debug.Log("レールに侵入した");
        Debug.Log("├侵入したレールの Type:ID =" + rail.gameObject.tag + " : " + rail.railId);
        Debug.Log("├目的地の Type:ID =" + nextRail + " : " + exeLog[lindex][1]);
        if (rail.gameObject.tag == nextRail)
        {
            if (rail.gameObject.tag == "Gate")
            {
                Debug.Log("└Check Clear");
                AddRP();
                undoHandler.AddUP(exeLog[lindex], currentFunc);
                nextFunc = (exeLog[lindex])[1];
                ExecuteRail(rail);
                UpdateCurrentFunc(nextFunc);
            }
            else if (rail.gameObject.tag == "Return")
            {
                Debug.Log("└Check Clear");
                undoHandler.AddUP(exeLog[lindex],GetCurrentRP(),GetCurrentRP().rpName);
                ExecuteRail(rail);
                UpdateCurrentFunc(nextFunc);
                RemoveRP();
            }
            else 
            {
                if (rail.railId == int.Parse(exeLog[lindex][1]))
                {
                    Debug.Log("└Check Clear");
                    undoHandler.AddUP(exeLog[lindex]);
                    ExecuteRail(rail);
                }
                else Debug.LogError("└Check Fail RailIDが一致しません");
            }
        }
        else
        {
            Debug.LogError("└Check Fail　目的地と一致しません");
            Debug.Log("-------------------------------------");
        }

        /*SKIP
     * ループ突入時、ログから今いるループのIDを取得してどこかに保存する
     * (option)合計ループ数と現在のループ数も取得したい(startの回数から取得できないかな)
     * SKIP時、同IDの終点パーツまで飛ぶ
     * 実行ログも終点の行まで飛ぶ（最初に出てきた同IDのbottom）
     * 多重ループはどうするの？
     * 分岐や関数も同じことできないかな
     */
    }
}
