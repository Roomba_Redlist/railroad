﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallSpeedReset : MonoBehaviour
{
    Rigidbody ballRig;
    public Vector3 speed =new Vector3(0,4f,4f);
    public bool hasSlowdown = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(ballRig);
    }
    //void OnCollisionExit(Collision other)
    //{
    //    ballRig.AddForce(new Vector3(0, 3f, 3f), ForceMode.VelocityChange);
    //    Debug.Log("Ball Collision Released");
    //}    
    void OnTriggerEnter(Collider other)
    {
        ballRig = other.gameObject.GetComponent<Rigidbody>();
        Debug.Log("Ball Collision Enter");
        ballRig.velocity = speed;
        if (hasSlowdown) Time.timeScale = Time.timeScale * 2f;
    }
    void OnTriggerStay(Collider other)
    {

        //Debug.Log("Ball Collision Stay");
    }
}
