﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallStopper : MonoBehaviour
{
    Rigidbody ballRig;
    public Vector3 speed =new Vector3(0,4f,4f);
    // Start is called before the first frame update

    //void OnCollisionExit(Collision other)
    //{
    //    ballRig.AddForce(new Vector3(0, 3f, 3f), ForceMode.VelocityChange);
    //    Debug.Log("Ball Collision Released");
    //}    
    void OnTriggerEnter(Collider other)
    {
        ballRig = other.gameObject.GetComponent<Rigidbody>();
        ballRig.AddForce(speed * this.transform.localScale.z, ForceMode.VelocityChange);
        Debug.Log("Ball Collision Enter");
    }
    void OnTriggerStay(Collider other)
    {

    }
}
