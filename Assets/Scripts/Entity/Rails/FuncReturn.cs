﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Cinemachine;

public class FuncReturn : FuncGate
{
    GameObject targetFunc;
    public override void WarpFunc(string distName)
    {
        distination = GameObject.Find(distName).transform.GetChild(0).transform.Find("Gate").gameObject;
        BallBase ballComp = ball.GetComponent<BallBase>();
        if (ballComp.returnPoint.Count==0) Debug.LogError("エラー:RPがない状態でreturn");
        if (ballComp.returnPoint.Count != 0 && ballComp.currentFunc != "main")
        {
            targetFunc = GameObject.Find(ballComp.returnPoint[ballComp.returnPoint.Count - 1].rpName);
            UpdateAllRangeCam();
            //ballComp.UpdateCurrentFunc(ballComp.returnPoint[ballComp.returnPoint.Count - 1].rpName);
            StartCoroutine(TransportBall(ballComp, this.transform.position, ballComp.GetCurrentRP().position)); 
        }
    }
}
