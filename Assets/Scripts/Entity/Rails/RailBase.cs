﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RailBase : MonoBehaviour
{
    public int railId=0;
    public int nest { get; set; }
    public int width { get; set; }
    public int length { get; set; }
    public RailType type { get; set; }
    public Transform startPoint;

    // Start is called before the first frame update
    void Start()
    {
        //Debug.Log(this.name +" : "+ railId);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag== "Player")
        {
            Debug.Log(name + " : Entred");
            other.gameObject.GetComponent<BallBase>().CheckRail(this);
        }
    }

    public virtual void ExecuteRailAction(bool state)
    {
        Debug.Log("execute");
        return;
    }
}