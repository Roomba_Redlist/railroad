﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuideLineControl : MonoBehaviour
{
    [SerializeField]
    float fadeinTime, fadeoutTime, keepTime;
    List<Vector3> positions;
    int subdivisionCount=32;
    LaunchBall launch;
    Vector3 velocity, startPos, endPos;
    public GameObject trailball;
    List<MeshRenderer> trailColors;
    [SerializeField]
    Color defaultColor,highlightedColor;

    // Start is called before the first frame update

    public void SetGuideLine(Vector3 velocity, Vector3 startPos, Vector3 endPos)
    {
        trailColors = new List<MeshRenderer>();
        Vector3[] positions = CalcPositions(velocity,startPos,endPos);
        //foreach (Vector3 pos in positions)
        //{
        //    Debug.Log(pos);
        //}
        foreach(Vector3 pos in positions)
        {
            GameObject trailobject = Instantiate(trailball);
            trailobject.transform.position = pos;
            MeshRenderer r = trailobject.GetComponent<MeshRenderer>();
            trailColors.Add(r);
            r.material.color = defaultColor;
        }
    }

    Vector3[] CalcPositions(Vector3 velocity, Vector3 startPos, Vector3 endPos)
    {
        this.velocity = velocity;
        List<float> time = new List<float>();
        List<Vector3> positions = new List<Vector3>();
        float landingTime = -2.0f * velocity.y / Physics.gravity.y;
        RaycastHit hit;
        for (int i = 1; i <= subdivisionCount; i++)
        {
            float timeProt = (landingTime / subdivisionCount)*i;
            time.Add(timeProt);
        }
        foreach (float timeProt in time)
        {
            Vector3 position = startPos　+ (velocity * timeProt) + (Physics.gravity*Mathf.Pow(timeProt, 2)/2);
            positions.Add(position);

        }
        for (int i=0; i <= positions.Count; i++)
        {

            if (Physics.Linecast(positions[i], positions[i + 1], out hit))
            {
                positions.RemoveRange(i + 1, positions.Count - (i+1));
                //positions.Add(hit.transform.position);
                i++;
                break;
            }
        }
        return positions.ToArray();
    }
    public IEnumerator ChangeTrailColor()
    {
        Debug.Log("Change Trail Color");
        float timer = 0f;
        while (timer < fadeinTime)
        {
            foreach (MeshRenderer renderer in trailColors)
            {
                renderer.material.color = Color.Lerp(renderer.material.color, highlightedColor, 0.1f / fadeinTime);
            }
            timer += Time.deltaTime;
            yield return new WaitForSeconds(0.016f);
        }
        timer = 0;
        yield return new WaitForSeconds(keepTime+velocity.magnitude/5f);
        while (timer < fadeoutTime) {
            foreach (MeshRenderer renderer in trailColors)
            {
                renderer.material.color = Color.Lerp(renderer.material.color, defaultColor, 0.1f / fadeoutTime);
            }
            timer += Time.deltaTime;
            yield return new WaitForSeconds(0.016f);
        }

    }
}
