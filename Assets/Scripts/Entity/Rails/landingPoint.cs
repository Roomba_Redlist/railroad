﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class landingPoint : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        Rigidbody ball = other.GetComponent<Rigidbody>();
        if (ball.velocity.magnitude>=20f)
        {
            ball.velocity = new Vector3(0, 0, 0);
        }
    }
}
