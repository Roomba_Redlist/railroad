﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BLoopRail : RailBase
{
    [SerializeField]
    GameObject flipper;
    public override void ExecuteRailAction(bool state)
    {
        Debug.Log("execute : "+state);
        if (state == false)
        {
            StartCoroutine(RotateFlipper(60.0f));
        }
        else
        {
            StartCoroutine(RotateFlipper(0.0f));
        }

        return;
    }
    IEnumerator RotateFlipper(float targetAngle)
    {
        Transform flipperAngle = flipper.transform;
        for (int i = 0; i <= 20; i++)
        {
            yield return new WaitForSeconds(0.01f);
            //ballPhys.velocity = new Vector3(0, 0, 0);
            flipperAngle.localRotation = Quaternion.Lerp(flipperAngle.localRotation, Quaternion.Euler(targetAngle, 0f, 0.0f), 0.45f);
        }
        flipperAngle.localRotation = Quaternion.Euler(targetAngle, 0.0f, 0.0f);
    }
}
