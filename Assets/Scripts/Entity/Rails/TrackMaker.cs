﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Cinemachine;

public class TrackMaker : MonoBehaviour
{
    public Transform StartPoint;
    public int courceLength=1;
    // Start is called before the first frame update
    private void Start()
    {
        //MakeTrack();

    }

    public void MakeTrack()
    {
        CinemachinePath track=GetComponent<CinemachinePath>();
        
        Transform min=null,max=null;
        var children = new Transform[this.transform.childCount];
        int i = 0;
         //子オブジェクトを順番に配列に格納
        foreach (Transform child in this.transform)
        {
            children[i++] = child;
            //Debug.Log(child);
        }
        min = children[0];
        max = children[0];
        foreach (Transform child in children)
        {
            if (child.position.z < min.position.z)
            {
                //Debug.Log("min updated");
                min = child;
            }
            if (child.position.z > max.position.z)
            {
                //Debug.Log("max updated");
                max = child;
            }
        }
        //Debug.Log(min + " min:max " + max);
        
        if (min != null && max != null)
        {


            track.m_Waypoints[0].position = min.localPosition;
            track.m_Waypoints[1].position = max.localPosition;
            track.m_Waypoints[0].tangent = min.localPosition * 0.1f;
            track.m_Waypoints[1].tangent = max.localPosition * 0.1f;
        }
        //track.m_Waypoints[0].position = StartPoint.localPosition;
        //track.m_Waypoints[1].position = this.transform.localPosition;
        //track.m_Waypoints[0].tangent = StartPoint.localPosition * 0.1f;
        //track.m_Waypoints[1].tangent = this.transform.localPosition;
    }

}
