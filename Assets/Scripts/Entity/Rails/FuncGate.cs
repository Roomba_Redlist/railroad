﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Cinemachine;

public class FuncGate : RailBase
{
    private bool IsGateActive;
    public GameObject distination, ball, camTarget,ring;
    public int GateID = 99,subdivision = 8;
    public string parentFuncName;
    public TextMeshProUGUI testmesh;
    public float stopTime = 0.2f;
    public List<GameObject> tunnelRings; 

    // Start is called before the first frame update
    void Start()
    {
        IsGateActive = true;
        Init();

        for(int i = 1; i < subdivision; i++)
        {
            GameObject _obj = Instantiate(ring);
            tunnelRings.Add(_obj);
            _obj.SetActive(false);
        }
    }

    public void Init()
    {
        //GateID = id;
        //funcName = name;
        parentFuncName = this.transform.root.name;
        //this.name = parentFuncName;
    }
    public void GateDeactivate()
    {
        IsGateActive = false;
    }

    public void GateActivate()
    {
        IsGateActive = true;
    }

    public override void ExecuteRailAction(bool state)
    {
        if (IsGateActive)
        {
            WarpFunc(ball.GetComponent<BallBase>().nextFunc);
        }
    return;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            //Debug.Log(name + " : Entred");
            ball = other.gameObject;
            other.gameObject.GetComponent<BallBase>().CheckRail(this);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        GateActivate();
    }
    public virtual void WarpFunc(string distName)
    {
        distination = GameObject.Find(distName).transform.GetChild(0).transform.Find("Gate").gameObject;
        //Debug.Log("Fade In End");
        //Debug.Log(dname);
        //Debug.Log(GameObject.Find(dname).name);
        //Debug.Log(GameObject.Find(dname).transform.Find("Start").name);
        //Debug.Log(GameObject.Find(dname).transform.Find("Start").transform.Find("Gate").name);
        if (distination != null)
        {
            BallBase ballComp = ball.GetComponent<BallBase>();
            Debug.Log("関数を移動：" + ballComp.currentFunc + " -> " + distName);
            StartCoroutine(TransportBall(ballComp, this.transform.position, distination.transform.position));
        }
        else
        {
            Debug.LogError("エラー：関数ゲートの遷移先が設定されていない");
        }

        
    }
    /// <summary>
    /// ボールをwaypoint方式でstartからgoalまで動かす。移動中はコライダとrigidbodyが無効化
    /// 軌道はU←こんな感じ
    /// </summary>
    /// <param name="ball"></param>
    /// <param name="start"></param>
    /// <param name="goal"></param>
    protected IEnumerator TransportBall(BallBase ball ,Vector3 start,Vector3 goal)
    {
        List<Vector3> tunnelPoints = new List<Vector3>();
        Vector3 length = goal-start;
        ball.GetComponent<Rigidbody>().isKinematic = true;
        ball.GetComponent<Collider>().enabled = false;
        UndoHandler undoHandler=GameObject.Find("UndoHandler").GetComponent<UndoHandler>();

        //プーリング化
        //トンネル出現からの時間計算でなく、終点到着で非表示に
        undoHandler.isAbleUndo = false;
        //中間地点を作る
        tunnelPoints.Add(start);
        tunnelPoints.Add(start + new Vector3(0, -2, 0));
        for(int i = 1; i < subdivision; i++)
        {
            Vector3 current= start + (length / subdivision * i);
            current.y -= 2;
            tunnelPoints.Add(current);
            //Debug.Log("vector3s");
        }
        tunnelPoints.Add(goal+new Vector3(0,-2,0));
        tunnelPoints.Add(goal);

        //トンネルを作る
        for (int j = 0; j < tunnelRings.Count; j++)
        {
            tunnelRings[j].transform.position = tunnelPoints[j+2];
            tunnelRings[j].transform.rotation = Quaternion.LookRotation(tunnelPoints[j+2] - tunnelPoints[j+1], Vector3.up);
            tunnelRings[j].SetActive(true);
        }


        foreach (Vector3 vector in tunnelPoints)
        {
            Debug.DrawLine(vector, new Vector3(vector.x, vector.y + 10, vector.z), Color.green, 2.0f);
        }

        //ボールを動かす
        distination.GetComponent<FuncGate>().GateDeactivate();
        UpdateAllRangeCam();
        for (int i = 1; i <= tunnelPoints.Count - 1; i++)
        {
            Vector3 point2point =  tunnelPoints[i]- tunnelPoints[i - 1];
            float point2pointLength = point2point.magnitude;
            float speed = 2f;
            for (float moveValue = 0; point2pointLength > moveValue; moveValue+=point2point.magnitude*Time.deltaTime*speed)
            {
                ball.transform.position += point2point*Time.deltaTime*speed;
                yield return null;
            }
            ball.transform.position = tunnelPoints[i];
        }
        ball.transform.position = goal;

        //ボール移動後の処理
        ball.GetComponent<Rigidbody>().isKinematic = false;
        ball.GetComponent<Collider>().enabled = true;
        undoHandler.isAbleUndo = true;
        foreach (GameObject _ring in tunnelRings)
        {
            _ring.SetActive(false);
        }
    }

    protected void UpdateAllRangeCam()
    {
        CinemachinePath newTarget = distination.transform.root.GetComponent<CinemachinePath>();
        CinemachineTrackedDolly dollyCamera=GameObject.Find("AllRangeCamera").
            GetComponent<CinemachineVirtualCamera>().
            GetCinemachineComponent<CinemachineTrackedDolly>();

        dollyCamera.m_Path = newTarget;
        dollyCamera.m_PathPosition = 0.5f;
    }

}
