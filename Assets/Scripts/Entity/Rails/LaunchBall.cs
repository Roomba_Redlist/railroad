﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaunchBall : MonoBehaviour
{
    [SerializeField]
    float slowness = 0.1f;
    public Vector3 velocity;
    public Vector3 targetPos = new Vector3(0, 0, 0);
    [SerializeField]
    GuideLineControl lineControl;

    SpeedManager speedManager;
    // Start is called before the first frame update
    void Start()
    {
        velocity = CalculateVelocity(this.transform.position, targetPos + new Vector3(0, 0, -0.1f), 60);
        lineControl.SetGuideLine(velocity,this.transform.position,targetPos);

        speedManager = GameObject.Find("SpeedManager").GetComponent<SpeedManager>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnTriggerEnter(Collider other)
    {
        //speedManager.isAbleTimeStop=false;
        Rigidbody ballPhys = other.GetComponent<Rigidbody>();
        //Transform startPoint = this.GetComponentInParent<RailBase>().startPoint;
        Debug.Log(targetPos);
        Vector3 startPos = other.transform.position;
        StartCoroutine(lineControl.ChangeTrailColor());
        StartCoroutine(Launch( ballPhys, velocity, startPos));

    }
    IEnumerator Launch(Rigidbody ballPhys,Vector3 velocity, Vector3 pos)
    {
        ballPhys.AddForce(2* Physics.gravity, ForceMode.Acceleration);
        ballPhys.velocity = new Vector3(0, 0, 0);
        ballPhys.position = pos;
        yield return new WaitForSeconds(0.6f);
        ballPhys.velocity = new Vector3(0, 0, 0);
        ballPhys.position = pos;
        ballPhys.AddForce(velocity, ForceMode.VelocityChange);
    }

    /// <summary>
    /// https://qiita.com/_udonba/items/a71e11c8dd039171f86c　より引用
    /// </summary>
    /// <param name="pointA"></param>
    /// <param name="pointB"></param>
    /// <param name="angle"></param>
    /// <returns></returns>
    private Vector3 CalculateVelocity(Vector3 pointA, Vector3 pointB, float angle)
    {
        // 射出角をラジアンに変換
        float rad = angle * Mathf.PI / 180;

        // 水平方向の距離x
        float x = Vector2.Distance(new Vector2(pointA.x, pointA.z), new Vector2(pointB.x, pointB.z));

        // 垂直方向の距離y
        float y = pointA.y - pointB.y;

        // 斜方投射の公式を初速度について解く
        float speed = Mathf.Sqrt(-Physics.gravity.y * Mathf.Pow(x, 2) / (2 * Mathf.Pow(Mathf.Cos(rad), 2) * (x * Mathf.Tan(rad) + y)));

        if (float.IsNaN(speed))
        {
            // 条件を満たす初速を算出できなければVector3.zeroを返す
            return Vector3.zero;
        }
        else
        {
            return (new Vector3(pointB.x - pointA.x, x * Mathf.Tan(rad), pointB.z - pointA.z).normalized * speed);
        }
    }
}
