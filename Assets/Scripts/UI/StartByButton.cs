﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class StartByButton : MonoBehaviour
{
    public TMP_InputField logInputField, courseInputField, logExtention, courceExtention;
    //public string logFile,courceFile;
    [SerializeField]
    RailBuilder railBuilder;
    [SerializeField]
    AnimPlayer animPlayer;
    [SerializeField]
    GameObject manager;
    
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void StartSimulation()
    {

        railBuilder.fileName = courseInputField.text;
        railBuilder.fileExtension = courceExtention.text;
        animPlayer.fileName = logInputField.text;
        animPlayer.fileExtension = logExtention.text;
        manager.SetActive(true);
        //railBuilder.StartSimulation();
        transform.parent.gameObject.SetActive(false);
    }
}
