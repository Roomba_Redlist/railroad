﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;
using UnityEngine.EventSystems;
using Cinemachine;
public class WindowSystem : MonoBehaviour, IDragHandler, IEndDragHandler, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler
{
    [SerializeField]
    CinemachineVirtualCamera virtualCamera;
    CinemachineTransposer transposer=null;
    CinemachineTrackedDolly dolly=null;
    private Vector3 zoomedOffset,minOffset;
    RectTransform rect;
    bool isHoverOnWindow,
        isDragging,
        isDraggableX,
        isDraggableY,
        isMaximizing;
    Vector3 dragPos;
    Vector2 mouseDelta;
    [SerializeField]
    Camera screenCam;
    [SerializeField]
    float magnitude = 35;
    [SerializeField]
    float zoomMp;
    Vector2 mousePoint,
            localPoint,
            normalizedPoint;
    PointerEventData pointer;
    Vector2[] prevRect;
    [SerializeField]
    bool hasAllRangeCam, hasCMCam=true;


    // Start is called before the first frame update
    public void Start()
    {
        if (hasAllRangeCam&&hasCMCam)
        {
            dolly = virtualCamera.GetCinemachineComponent<CinemachineTrackedDolly>();
            //Debug.Log("dolly:"+this);
            minOffset = dolly.m_PathOffset * 0.03f;
            dolly.m_PathPosition = 0.5f;
        }
        else
        {
            if (hasCMCam)
            {
                transposer = virtualCamera.GetCinemachineComponent<CinemachineTransposer>();
                //Debug.Log("trans:" + this);
                minOffset = transposer.m_FollowOffset * 0.03f;
            }
        }

        rect = GetComponent<RectTransform>();
        //Debug.Log(rect);
        localPoint = Vector2.zero;
        //Debug.Log(rect.anchorMin);
        isMaximizing = false;
        prevRect = new Vector2[4];

    }

    // Update is called once per frame
    public void Update()
    {
        float mouseWheel = Input.GetAxis("Mouse ScrollWheel");
        //Debug.Log(mouseWheel);
        if(hasCMCam)
        Zoom(-mouseWheel);
        mousePoint = Input.mousePosition;
        if (isHoverOnWindow)
        {
            if (!isDragging)
            {
                RectTransformUtility.ScreenPointToLocalPointInRectangle(rect, mousePoint, screenCam, out localPoint);
                //ifの評価値はそのままboolに代入できる
                normalizedPoint = new Vector2(Mathf.Abs(localPoint.x),Mathf.Abs(localPoint.y)) / (rect.rect.size);
                isDraggableX = (normalizedPoint.x >= 0.9);
                isDraggableY = (normalizedPoint.y >= 0.9);
                //Debug.Log(rect.rect.size+"\n"+localPoint+"\nOn" + this.name + ": \n isDraggableX=" + isDraggableX + "\n isDraggableY=" + isDraggableY + "\n localPos=" + normalizedPoint);
            }
        }

    }

    public void Zoom(float scrollValue)
    {
        if (isHoverOnWindow)
        {
            if (!hasAllRangeCam && hasCMCam)
            {
                zoomedOffset = transposer.m_FollowOffset + scrollValue * transposer.m_FollowOffset * zoomMp;
                if (scrollValue != 0 && (zoomedOffset.y - minOffset.y >= 0))
                {
                    transposer.m_FollowOffset = zoomedOffset;
                }

                //一定距離に近づくまでカリングを行わない
                if (zoomedOffset.magnitude <= 0.2)
                {
                    virtualCamera.gameObject.GetComponent<Camera>().cullingMask = ~(1 << 10);
                }
                else
                {
                    virtualCamera.gameObject.GetComponent<Camera>().cullingMask = -1;
                }
            }
            else
            {
                zoomedOffset = dolly.m_PathOffset + scrollValue * dolly.m_PathOffset * zoomMp;
                if (scrollValue != 0 && (zoomedOffset.y - minOffset.y >= 0))
                {
                    //Debug.Log(dolly.m_PathOffset);
                    dolly.m_PathOffset = zoomedOffset;
                }

                //一定距離に近づくまでカリングを行わない
                if (zoomedOffset.magnitude <= 0.2)
                {
                    virtualCamera.gameObject.GetComponent<Camera>().cullingMask = ~(1 << 10);
                }
                else
                {
                    virtualCamera.gameObject.GetComponent<Camera>().cullingMask = -1;
                }

            }

        }
        
    }
    public void CloseAndOpenWindow()
    {
        gameObject.SetActive(!gameObject.activeSelf);
        gameObject.transform.SetSiblingIndex(100);
    }
    public void ToggleMaximizeWindow()
    {
        if (isMaximizing)
        {
            isMaximizing = false;

            rect.anchorMin = prevRect[0];
            rect.anchorMax = prevRect[1];
            rect.offsetMin = prevRect[2];
            rect.offsetMax = prevRect[3];

        }
        else
        {
            isMaximizing = true;

            prevRect[0] = rect.anchorMin;
            prevRect[1] = rect.anchorMax;
            prevRect[2] = rect.offsetMin;
            prevRect[3] = rect.offsetMax;

            rect.anchorMin = new Vector2(0, 0);
            rect.anchorMax = new Vector2(1, 1);
            rect.offsetMin = new Vector2(0, 33.5f);
            rect.offsetMax = new Vector2(0, 0);
        }
    }
    public void OnPointerDown(PointerEventData eventData)
    {
        //Debug.Log("selected" + name);
        transform.SetAsLastSibling();
    }
    public void OnDrag(PointerEventData eventData)
    {
        mouseDelta = magnitude * (new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y")));
        if (Input.GetMouseButton(0))
        {

            //Debug.Log("Dragging");
            if (!isDragging)
            {
                isDragging = true;
                dragPos = new Vector2(rect.localPosition.x, rect.localPosition.y) - eventData.position;

            }

            int x, y;
            if (isDraggableX || isDraggableY)
            {
                if (localPoint.x >= 0)
                {
                    x = 1;
                }
                else
                {
                    x = -1;
                }
                if (localPoint.y >= 0)
                {
                    y = 1;
                }
                else
                {
                    y = -1;
                }
                if (isDraggableX) StretchWindow(x*mouseDelta.x, 0);
                if (isDraggableY) StretchWindow(0, y*mouseDelta.y);
            }
            else
            {
                //Debug.Log(rect);

                rect.localPosition = new Vector3(dragPos.x + eventData.position.x, dragPos.y + eventData.position.y, 0);
            }
        }else if (Input.GetMouseButton(1))
        {
            if (hasAllRangeCam && hasCMCam)
            {
                //Debug.Log(mouseDelta);
                dolly.m_PathPosition = Mathf.Clamp(dolly.m_PathPosition+mouseDelta.x*0.0005f+ mouseDelta.y*0.0005f, 0.005f, 1f);
            }
        }
    }
    private void StretchWindow(float x, float y)
    {
        //Debug.Log("mouse X=" + x + "mouse Y=" + y);
        if ((rect.rect.width <= 180 && x < 0) || (rect.rect.width >= 1920 && x > 0)) x = 0;
        if ((rect.rect.height <= 180 && y < 0) || (rect.rect.height >= 1024 && y > 0)) y = 0;
        rect.sizeDelta += new Vector2(x, y);
    }
    public void OnEndDrag(PointerEventData eventData)
    {
        isDragging = false;
        //Debug.Log("Dropped");
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        isHoverOnWindow = true;
        //Debug.Log("hover on");
    }
    public void OnPointerExit(PointerEventData eventData)
    {
        isHoverOnWindow = false;
        //Debug.Log("hover off");
    }
    //public void OnSelect(BaseEventData eventData)
    //{
    //    //foreach (GameObject window in GameObject.FindGameObjectsWithTag("Window"))
    //    //{
    //    //    window.transform.position = new Vector3 (window.transform.position.x, window.transform.position.y, window.transform.position.y+1);
    //    //    this.transform.position = new Vector3 (transform.position.x, transform.position.x, 0);
    //    //}
    //    Debug.Log("selected"+name);
    //    transform.SetAsLastSibling();
    //}
}