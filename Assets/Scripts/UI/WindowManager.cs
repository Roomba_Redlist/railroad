﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class WindowManager : MonoBehaviour
{
    public BallBase ball;
    public TMP_Text timeUI,linesUI;
    public SpeedManager speedManager;
    float worldTime=0,speedMp;
    private void Start()
    {
        speedManager = GameObject.Find("SpeedManager").GetComponent<SpeedManager>();
        speedMp = speedManager.speedMp;
    }

    private void Update()
    {
        worldTime += Time.deltaTime/speedMp;
        timeUI.text = "Time:" + ((Mathf.Floor(worldTime*100)/100).ToString());
        linesUI.text = ball.lindex.ToString()+" / " + ball.exeLog.Count;
    }
}
