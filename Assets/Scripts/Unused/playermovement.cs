﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playermovement : MonoBehaviour
{
    Vector2 moveInput;
    bool isLanding;
    Rigidbody rb;
    [SerializeField]
    float speed;
    float jumpPower;
    float jumpPowerMax;
    [SerializeField]
    float jumpSpeed;
    public enum PlayerState
    {
        Neutral,
        Jump
    }
    PlayerState playerState;

    // Start is called before the first frame update
    void Start()
    {
        playerState = PlayerState.Neutral;
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(isLanding);
        moveInput = new Vector2(
            Input.GetAxis("Horizontal"),
            Input.GetAxis("Vertical")
            );
        rb.velocity = new Vector3(moveInput.x * speed, rb.velocity.y, moveInput.y * speed);
        if (isLanding&&Input.GetButtonDown("Jump"))
        {
            playerState = PlayerState.Jump;

        }
        if(playerState==PlayerState.Jump)
        {
            jumpPower++;
            if (jumpPowerMax>=20||Input.GetButtonUp("Jump"))
            {
                playerState = PlayerState.Neutral;
            }
        }
        if (jumpPower > 0)
        {
            rb.AddForce(Vector3.up*jumpSpeed,ForceMode.Acceleration);
            jumpPower--;
            Debug.Log(jumpPower);
        }

    }
    private void OnTriggerStay(Collider other)
    {
        isLanding = true;

    }
    private void OnTriggerExit(Collider other)
    {
        isLanding = false;
    }

}
