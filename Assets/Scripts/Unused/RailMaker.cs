﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System;
using UnityEngine;

/*
０、生成中、Branchが来たら幅逆算開始。
１、作りたいコースのThen側は幅逆算に要らないので、作りたいコースと同じネストのElseが来るまで待つ
２、Elseがきたら再起関数を実行。
３、Endが来たら終了。再帰関数から得た幅を返す。もしElseがなかった場合、中にはなにもないので幅は0。

再帰関数の中身
・引数は定義ファイル、読んでいる行数、ネスト数の３つ。
・１つ下のネストのStartが来たら再帰関数実行。受け取った幅は最大幅として記憶
・もういちど１つ下のネストのStartが来たら、同ネスト同士は直列に並んでいることを意味する。
・同じネストのElseが来たら、さっきの「もう一度１つ下のネストが来たら」という条件をリセット。
・同じネストのEndが来たら終了。最大幅に＋１(自身の幅)して返す。 

*/
public enum RailType
{
    Other,
    Branch,
    BranchHasElse,
    Loop,
    Func
}

public class ReservedParts{
    public GameObject part { get; set; }
    public int id { get; set; }
    public int nest { get; set; }
    public int width { get; set; }
    public int length { get; set; }
    public int lengthBalance { get; set; }
    public RailType type { get; set; }
}


public class RailMaker : MonoBehaviour
{


    [TextArea()]
    public string csv;
    int i = 0;
    int j = 0;
    int railId = 0;
    int structureId = 0;
    List<List<string>> courceDef;//制御構造の種類,(ネストの深さ),"原文"
    int lindex = 0;

    int width;
    Vector2Int lengthstat;
    int length,lengthBalance;
    int LengthAll;
    bool hasElse =false;

    [SerializeField]
    GameObject animPlayer,basic,brank,brBase, brConnect, brThenStart, brThenEnd, brEnd, alpStart, alpEnd, alpBypass, alpElev, blpStart, blpEnd, blpBypass, blpElev;
    [SerializeField]
    GameObject builder;
    List<ReservedParts> reserved;
    List<GameObject> Rails;
    [SerializeField]
    GameObject maincam;
    Transform builderTransform;
    Transform elseTransform;
    // Start is called before the first frame update
    IEnumerator Start()
    //voidStart()
    {
        FileInfo fi = new FileInfo(Application.dataPath + "/" + "courcedef.txt");
        try
        {
            using (StreamReader sr = new StreamReader(fi.OpenRead(), Encoding.UTF8))
            {
                csv = sr.ReadToEnd();
            }
        }
        catch (Exception e)
        {
            Debug.Log("error!file reading failured\n");
        }
        reserved = new List<ReservedParts>();
        builderTransform = transform;
        courceDef = new List<List<string>>();
        string[] splittedCsv = csv.Split('\n');
        LengthAll = LengthCheck(courceDef,0,0).x;
        maincam.transform.position = new Vector3(length * 1, length*1,length*1);
        foreach (string str in splittedCsv)
        {
            courceDef.Add(new List<string>());
            foreach (string word in str.Split(','))
            {
                courceDef[i].Add(word);
            }
            foreach (string st in courceDef[i])
            {
                //Debug.Log(st + ":");
            }
            //Debug.Log("\n----line end----");
            i++;
        }

        foreach (List<string> line in courceDef)
        {
            //Debug.Log(string.Join(",",line));
            //Debug.Log(lindex);
            if (line.Contains("other"))
            {
                Build(basic);
            }
            if (line.Contains("branch_start"))
            {

                width = WidthCheck(courceDef, lindex+1, Nest(line));
                lengthstat = LengthCheck(courceDef, lindex, Nest(line));
                //Debug.Log("lengthstat = LengthCheck(courceDef, " + lindex + ", " + Nest(line) + ");");
                length = lengthstat.x;
                lengthBalance = lengthstat.y;
                Debug.Log("lengthbalance = " + lengthBalance);
                GameObject currentRail = Build(brBase);
                AddReserve(currentRail, Nest(line), width, length, lengthBalance, RailType.Branch);
                currentRail.GetComponent<RailBase>().width = width;
                currentRail.GetComponent<RailBase>().railId = reserved.Last().id;
                currentRail.GetComponent<RailBase>().type = reserved.Last().type;
                for (int i = 1; i < reserved.Last().width; i++)
                {
                    Build(brConnect);
                }
                Build(brThenStart);
            }
            if (line.Contains("branch_else") && Nest(line) == reserved.Last().nest)
            {
                for (int i = 0; i > reserved.Last().lengthBalance; i--)
                {
                    Build(brank);
                }
                Build(brThenEnd);
                for (int i = 1; i < reserved.Last().width; i++)
                {
                    Build(brConnect);
                }
                MoveBuilder(reserved.Last().part.transform.Find("ElseSideEnd"));
                reserved.Last().type = RailType.BranchHasElse;
                Debug.Log("this is else");
            }
            if (line.Contains("branch_end") && Nest(line) == reserved.Last().nest)
            {
                int width = reserved.Last().width;
                if (reserved.Last().type != RailType.BranchHasElse)
                {
                    Build(brThenEnd);
                    for (int i = 1; i < reserved.Last().width; i++)
                    {
                        Build(brConnect);
                    }
                    MoveBuilder(reserved.Last().part.transform.Find("ElseSideEnd"));
                }
                for (int i = 0; i < reserved.Last().lengthBalance; i++)
                {
                    Build(brank);
                }
                Build(brEnd);

                RemoveReserve(reserved);
                hasElse = false;
            }

            if (line.Contains("aloop_start"))
            {
                //int width;
                int length;
                //width = WidthCheck(courceDef, lindex, Nest(line));
                length = LengthCheck(courceDef, lindex, Nest(line)).x;
                //Debug.Log("width of Building Rail=" + width);
                //Debug.Log("length of Building Rail=" + length);
                GameObject currentRail = Build(alpStart);
                AddReserve(currentRail, Nest(line), 1, length, 0, RailType.Loop);
                Debug.Log(currentRail);
                currentRail.GetComponent<RailBase>().width = width;
                currentRail.GetComponent<RailBase>().railId = reserved.Last().id;
                currentRail.GetComponent<RailBase>().type = reserved.Last().type;
            }
            if (line.Contains("aloop_end") && Nest(line) == reserved.Last().nest)
            {
                GameObject currentRail = Build(alpEnd);
                currentRail.transform.Find("Bypass").localScale = new Vector3(1, 1, ((reserved.Last().length-2) * 4.15f) +4f);
                reserved.Last().part.transform.Find("Elevator").localScale = new Vector3(1, 1, ((reserved.Last().length-2)* 3.13f) +2.8f);
                currentRail.GetComponent<RailBase>().railId = reserved.Last().id;
                currentRail.GetComponent<RailBase>().type = reserved.Last().type;
                RemoveReserve(reserved);
            }

            if (line.Contains("bloop_start"))
            {
                //int width;
                int length;
                //width = WidthCheck(courceDef, lindex, Nest(line));
                length = LengthCheck(courceDef, lindex, Nest(line)).x;
                //Debug.Log("width of Building Rail=" + width);
                //Debug.Log("length of Building Rail=" + length);
                GameObject currentRail = Build(blpStart);
                AddReserve(currentRail, Nest(line), 1, length, 0, RailType.Loop);
                Debug.Log(currentRail);
                currentRail.GetComponent<RailBase>().width = width;
                currentRail.GetComponent<RailBase>().railId = reserved.Last().id;
                currentRail.GetComponent<RailBase>().type = reserved.Last().type;
            }
            if (line.Contains("bloop_end") && Nest(line) == reserved.Last().nest)
            {
                GameObject currentRail = Build(blpEnd);
                Debug.Log("length-2 = "+(reserved.Last().length - 2));
                currentRail.transform.Find("Elevator").localScale = new Vector3(1, 1, ((reserved.Last().length - 2) * 2.3f) + 1.1f);
                reserved.Last().part.transform.Find("Bypass").localScale = new Vector3(1, 1, ((reserved.Last().length - 2) * 2.5f) + 3.2f);
                currentRail.transform.Find("RElevator").localScale = new Vector3(1, 1, ((reserved.Last().length - 2) * 2.65f) + 1.1f);
                reserved.Last().part.transform.Find("RBypass").localScale = new Vector3(1, 1, ((reserved.Last().length - 2) * 4.4f) + 3.2f);
                currentRail.GetComponent<RailBase>().railId = reserved.Last().id;
                currentRail.GetComponent<RailBase>().type = reserved.Last().type;
                RemoveReserve(reserved);
            }

            lindex++;
            //Debug.Log(reserved.Last().width +"*"+ reserved.Last().length + " " + reserved.Last().lengthBalance + "," + reserved.Last().nest);
            yield return new WaitForSeconds(0.05f);
        }
        animPlayer.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {

    }

    void AddReserve(GameObject currentRail,int n, int w, int l, int lengthBalance,RailType type)
    {
        reserved.Add(new ReservedParts
        {
            id = structureId++,
            part = currentRail,
            nest = n,
            width = w,
            length = l,
            lengthBalance = lengthBalance,
            type = type
        });
        railId++;
    }

    void RemoveReserve(List<ReservedParts> reserved)
    {
        reserved.RemoveAt(reserved.Count - 1);
    }

    Transform MoveBuilder(Transform transform)
    {
        builderTransform = transform;
        return transform;
    }

    GameObject Build(GameObject target)
    {
        
        GameObject part = Instantiate(target, builderTransform.position, builderTransform.rotation);//パーツ設置。　位置、角度決めも同時に行う。
        builderTransform = part.transform.Find("End").transform;//設置したパーツの子要素から"End"と名のつくものを探して、Builderのtransformをそれに合わせる
        return part;//設置したパーツのインスタンスを返す
    }
    
    //バグあり。
    int WidthCheck(List<List<string>> def,int lindex,int nest)
    {
        int size = 0;
        List<List<string>> clippedDef = def.Skip(lindex).ToList();
        foreach (List<string> line in clippedDef)
        {
            //Debug.Log("widthCheck: "+string.Join(",", line));
            if (nest == Nest(line))
            {
                if (line.Contains("branch_else"))
                {
                    size = WidthCheck2(def, lindex + 1, Nest(line));
                }
                if (line.Contains("branch_end"))
                {
                    return size;
                }
            }
        }
        return size;
    }
    int WidthCheck2(List<List<string>> def, int lindex, int nest)
    {
        int size = 0;
        int sizeOfSide = 0;
        int maxSize = 0;
        List<List<string>> clippedDef = def.Skip(lindex).ToList();
        foreach (List<string> line in def)
        {
            if (nest +1 == Nest(line))
            {
                if (line.Contains("branch_start"))
                {
                    size = WidthCheck2(def, lindex + 1, Nest(line));
                    if (maxSize < size)
                    {
                        sizeOfSide = size;
                    }
                    size = 0;
                }
            }

            if (nest == Nest(line))
            {
                if (line.Contains("branch_else"))
                {
                    maxSize += sizeOfSide;
                    sizeOfSide = 0;
                }
                if (line.Contains("branch_end"))
                {
                    maxSize += sizeOfSide;
                    return maxSize+1;
                }
            }
        }
        Debug.Log("!終点が見つかりません!"+size);
        return maxSize;
    }

    Vector2Int LengthCheck(List<List<string>> def, int lindex, int nest)
    {
        bool hasElse=false;
        int size = (WidthCheck(def, lindex+1, nest)-1) * 2;//else側に加算されてる
        Debug.Log(size);
        if (size < 0)
        {
            size = 0;
        }
        int sizeOfThen = 0;
        List<List<string>> clippedDef = def.Skip(lindex).ToList();
        //Debug.Log("lengthcheck called");
        foreach (List<string> line in clippedDef)
        {
            //Debug.Log(lindex);
            //Debug.Log(line[0] + line[1]);
            //Debug.Log("lengthCheck: " + string.Join(",", line));
            if ((line.Contains("aloop_start") || line.Contains("aloop_end") || line.Contains("bloop_start") || line.Contains("bloop_end")) && nest != Nest(line))
            {
                size++;
            }
            if (nest + 1 == Nest(line))
            {
                if (line.Contains("branch_start"))
                {
                    size += LengthCheck(def, lindex + 1, Nest(line)).x;
                }
                if (line.Contains("other"))
                {
                    size++;
                    //Debug.Log("size:"+size);
                }
            }
            if (nest == Nest(line))
            {
                if (line.Contains("branch_else"))
                {
                    sizeOfThen = size;
                    size = 0;
                    hasElse = true;
                }
                if (line.Contains("branch_end"))
                {
                    if (!hasElse)
                    {
                        sizeOfThen = size;
                        size = 0;
                    }
                    //Debug.Log(sizeOfThen + ":" + size);
                    return new Vector2Int(Mathf.Max(size, sizeOfThen) + 2, sizeOfThen - size);
                }
                if(line.Contains("aloop_end") || line.Contains("bloop_end"))
                {
                    return new Vector2Int(Mathf.Max(size, size)+2, sizeOfThen - size);
                }
            }
            lindex++;
        }
        Debug.Log("EOF length = "+size);
        return new Vector2Int(Mathf.Max(size, sizeOfThen) + 2,sizeOfThen-size);
    }

    int Nest(List<string> line)
    {
        //Debug.Log(line[1]);
        return int.Parse(line[1]);
    }
    
}
