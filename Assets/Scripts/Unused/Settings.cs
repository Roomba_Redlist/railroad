﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "MyScriptable/Create Settings")]
public class Settings : ScriptableObject
{
    public Vector3 FPSAngle;
    public Vector3 FPSAngleDefault;
    public Vector3 behindAngle;
    public Vector3 behindAngleDefault;
    public Vector3 birdviewAngle;
    public Vector3 fullmapAngle;
}
