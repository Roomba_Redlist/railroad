﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlipOnEnter : MonoBehaviour
{
    [SerializeField]
    GameObject flipper;
    [SerializeField]
    Vector3 angle = new Vector3(35.0f, 0.0f, 0.0f);
    public int count=3,countDefault;
    bool state;
    Quaternion euler;
    private void Start()
    {
        euler = Quaternion.Euler(angle);
        countDefault = count;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (state == false&&count==1)
        {
            count = countDefault;
            flipper.transform.localRotation = euler;
            state = true;
        }
        else
        {
            count--;
            flipper.transform.localRotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
            state = false;
        }
        
    }
}

