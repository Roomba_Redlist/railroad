﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Byppass : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnCollisionStay(Collision collision)
    {
        int maxSpeed=5;
        Rigidbody targetRigid = collision.gameObject.GetComponent<Rigidbody>();
        float force = 20*(1-targetRigid.velocity.z/maxSpeed);
        targetRigid.AddForce(targetRigid.velocity*force, ForceMode.Acceleration);
        //Debug.Log("Ball Speed = " + targetRigid.velocity.z + " + " + force);
    }
}
