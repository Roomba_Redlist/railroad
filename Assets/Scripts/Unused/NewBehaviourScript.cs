﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour
{
    [TextArea()]
    public string csv;
    List<List<string>> courceDef;//制御構造の種類,(ネストの深さ),"原文"
    // Start is called before the first frame update
    void Start()
    {
        int i = 0;
        int lindex = 0;
        courceDef = new List<List<string>>();
        string[] splittedCsv = csv.Split('\n');
        foreach (string str in splittedCsv)
        {
            courceDef.Add(new List<string>());
            foreach (string word in str.Split(','))
            {
                courceDef[i].Add(word);
            }
            i++;
        }
        int width = 0;
        foreach (List<string> line in courceDef)
        {
            if (line.Contains("branch_start")&& Nest(line)==0)
            {
                width = WidthCheck(courceDef, lindex, Nest(line));
                //Debug.Log(courceDef.IndexOf(line));
                Debug.Log(width);
            }
            lindex++;
        }

    }
    // Update is called once per frame
    void Update()
    {
        
    }
    int WidthCheck(List<List<string>> def, int lindex, int nest)
    {
        int size = 0;
        List<List<string>> clippedDef = def.Skip(lindex).ToList();
        foreach (List<string> line in clippedDef)
        {

            if (nest == Nest(line))
            {
                if (line.Contains("branch_else"))
                {
                    //foreach (string st in line)
                    //{
                    //    Debug.Log(st + ":");
                    //}
                    //Debug.Log("recursivate parent," + lindex +","+ def.IndexOf(line) + "," + Nest(line));
                    size = WidthCheck2(def, lindex, Nest(line));
                }
                if (line.Contains("branch_end"))
                {
                    //Debug.Log("block end");
                    return size;
                }
            }
            //Debug.Log("lindex++ = "+lindex);
            lindex++;
        }
        Debug.Log("end not found");
        return size;
    }

    int WidthCheck2(List<List<string>> def, int lindex, int nest)
    {
        int size = 0;
        int sizeOfSide = 0;
        int maxSize = 0;
        List<List<string>> clippedDef = def.Skip(lindex).ToList();
        foreach (List<string> line in clippedDef)
        {
            if (nest + 1 == Nest(line))
            {
                size = 0;
                if (line.Contains("branch_start"))
                {
                    //foreach (string st in line)
                    //{
                    //    Debug.Log(st + ",");
                    //}
                    //Debug.Log("recursivate," + lindex + "," + def.IndexOf(line) + "," + Nest(line));
                    size = WidthCheck2(def, lindex, Nest(line));
                    if (maxSize < size)
                    {
                        sizeOfSide = size;
                    }
                }
            }

            if (nest == Nest(line))
            {
                if (line.Contains("branch_else"))
                {
                    maxSize += sizeOfSide;
                    sizeOfSide = 0;
                }
                if (line.Contains("branch_end"))
                {
                    //foreach (string st in line)
                    //{
                    //    Debug.Log(st + ":");
                    //}
                    maxSize += sizeOfSide;
                    return maxSize + 1;
                }
            }
            lindex++;
            //Debug.Log("lindex = " + lindex);
        }
        return 1000;
    }

    int Nest(List<string> line)
    {
        return int.Parse(line[1]);
    }
}
