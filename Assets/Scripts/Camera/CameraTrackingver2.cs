﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTracking_ver2 : MonoBehaviour
{
    public Vector3 currentPos;
    public Vector3 prevPos;
    public Vector3 delta;
    // Start is called before the first frame update
    void Start()
    {
        currentPos = transform.position;
        prevPos = transform.position;
    }
    void Update()
    {

    }
        // Update is called once per frame
    void LateUpdate()
    {
        currentPos = transform.position;

        delta = currentPos - prevPos;

        transform.rotation = Quaternion.LookRotation(delta, Vector3.up);

        prevPos = transform.position;
    }
}
