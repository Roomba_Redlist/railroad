﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllBehindView : MonoBehaviour
{
    [SerializeField]
    float Angle=60f;
    bool isCamControlled;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {
        Rigidbody ballPhys = other.GetComponent<Rigidbody>();
        CameraAngleManager manager = GameObject.Find("CameraAngleManager").GetComponent<CameraAngleManager>();
        if (!isCamControlled)
        {
            manager.StartCoroutine("Launch",ballPhys);
        }
    }
}
