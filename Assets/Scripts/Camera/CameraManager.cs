﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    [SerializeField]
    Camera camera;
    [SerializeField]
    GameObject ball;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        ball = GameObject.Find("Ball(Clone)");
        camera.transform.position = ball.transform.position;
    }
}
