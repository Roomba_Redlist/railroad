﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BehindView : MonoBehaviour
{
    [SerializeField]
    private Transform target;
    [SerializeField]
    private Vector3 offset;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        this.transform.position = target.position;
        this.transform.rotation = target.rotation;

    }
}
