﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraAngleManager : MonoBehaviour
{
    Transform cameraAngle,FPSAngle;
    Quaternion startAngle, targetAngle, FPSStartAngle, FPSTargetAngle;

    Settings settings;
    float lerpSteps = 0.1f;
    [SerializeField]
    float Angle = 60f;
    public bool isCamControlled;
    bool isFirstControl=false;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator Launch(Rigidbody ballPhys)
    {
        if (!isFirstControl)
        {
            cameraAngle = GameObject.Find("CamPivot").transform;
            startAngle = cameraAngle.rotation;
            targetAngle = cameraAngle.rotation * Quaternion.AngleAxis(Angle, Vector3.right);
            FPSAngle = GameObject.Find("FPSCameraNew").transform;
            FPSStartAngle = FPSAngle.rotation;
            FPSTargetAngle = Quaternion.Euler(60f, 0f, 0f);
            isFirstControl = true;
        }
        isCamControlled = true;
        Debug.Log(targetAngle);
        for (int i = 0; i <= 60; i++)
        {
            yield return new WaitForSeconds(0.01f);
            //ballPhys.velocity = new Vector3(0, 0, 0);
            cameraAngle.rotation = Quaternion.Lerp(cameraAngle.rotation, targetAngle, lerpSteps);
            cameraAngle.localScale = Vector3.Lerp(cameraAngle.localScale, new Vector3(3, 3, 3), lerpSteps);

            FPSAngle.rotation = Quaternion.Lerp(FPSAngle.rotation, FPSTargetAngle, lerpSteps);
        }
        cameraAngle.rotation = targetAngle;
        FPSAngle.rotation = FPSTargetAngle;

    }
    IEnumerator Reset(Rigidbody ballPhys)
    {
        Debug.Log("Camera reset");
        for (int i = 0; i <= 60; i++)
        {
            // scale = Mathf.Lerp(scale,1,Time.deltaTime*slowness);
            // Time.timeScale = startTime*scale;
            yield return new WaitForSeconds(0.01f);
            cameraAngle.rotation = Quaternion.Lerp(cameraAngle.rotation, startAngle, lerpSteps);
            cameraAngle.localScale = Vector3.Lerp(cameraAngle.localScale, new Vector3(1, 1, 1), lerpSteps);

            FPSAngle.rotation = Quaternion.Lerp(FPSAngle.rotation, FPSStartAngle, lerpSteps);
            
        }
        cameraAngle.rotation = startAngle;
        FPSAngle.rotation = FPSStartAngle;
        isCamControlled = false;
    }
}
