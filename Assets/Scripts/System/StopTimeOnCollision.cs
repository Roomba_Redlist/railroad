﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopTimeOnCollision : MonoBehaviour
{
    [SerializeField]
    public float stopDelay = 0.5f;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("Animation End");
        StartCoroutine(Stop(stopDelay));
    }

    IEnumerator Stop(float _delay)
    {
        yield return new WaitForSeconds(_delay);
        Debug.Log("Time Stopped");
        GameObject.Find("SpeedManager").GetComponent<SpeedManager>().SetTimeStop(true);
    }
}
