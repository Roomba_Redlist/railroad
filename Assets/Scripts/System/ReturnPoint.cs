﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace RailroadSystem
{
    public class ReturnPoint
    {
        public Vector3 position;
        public string rpName;

        public ReturnPoint(Vector3 pos, string st)
        {
            position = pos;
            rpName = st;
        }
    }
}
