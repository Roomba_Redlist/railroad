﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using UnityEngine;
using Cinemachine;

public class AnimPlayer : MonoBehaviour
{
    [TextArea()]
    public string csv;
    List<List<string>> exeLog = new List<List<string>>();
    [SerializeField, Tooltip("読み込むファイル名")]
    public string fileName;
    [SerializeField, Tooltip("読み込む拡張子")]
    public string fileExtension;
    [SerializeField]
    public CinemachineVirtualCamera TPS,BirdView,AllRange;

    [SerializeField]
    BallBase ball;
    // Start is called before the first frame update
    public void Start()
    {
        Debug.Log(name + "start");
        // ファイルパスを取得
        string filePath = Application.dataPath + @"/" + fileName + "." + fileExtension;
        // ファイルパスからファイルを読み取り、行の配列を取得
        string[] lines = File.ReadAllLines(filePath);

        int i = 0;

        //string[] splittedCsv = csv.Split('\n');

        foreach (string str in lines)
        {
            exeLog.Add(new List<string>());
            var splits = str.Split(',');
            if (splits.Length < 2)
            {
                continue;
            }
            foreach (string word in splits)
            {
                exeLog[i].Add(word);
            }

            foreach (string st in exeLog[i])
            {
                Debug.Log(st + ":");
            }
            Debug.Log("\n----line end----");
            i++;
        }


        BirdView.Follow = ball.gameObject.transform;
        TPS.LookAt = ball.gameObject.transform;
        AllRange.GetCinemachineComponent<CinemachineTrackedDolly>().m_Path = GameObject.Find("main").GetComponent<CinemachinePath>();
        AllRange.GetCinemachineComponent<CinemachineTrackedDolly>().m_PathOffset *= GameObject.Find("main").GetComponent<TrackMaker>().courceLength*0.5f;
        StartCoroutine("timer");

    }

    IEnumerator timer()
    {
        yield return new WaitForSeconds(1f);
        ball.gameObject.SetActive(true);
        ball.Activate(exeLog);
        ball.transform.position = GameObject.Find("main").transform.position + new Vector3(0, 0.0f, -0.0f);
        Time.timeScale = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

}
