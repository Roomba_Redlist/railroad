﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using System.Linq;
using UnityEngine.ProBuilder;
using UnityEngine.ProBuilder.MeshOperations;
using UnityEngine.UI;
using TMPro;

public class FileLineEntry
{
    private string typeName;
    private int nest;
    private string rawCode;
    private string funcName;

    public FileLineEntry(string type, int nestId, string rawCodeIn, string name)
    {
        typeName = type;
        nest = nestId;
        rawCode = rawCodeIn;
        funcName = name;
    }

    public string GetTypeName()
    {
        return typeName;
    }

    public int GetNest()
    {
        return nest;
    }

    public string GetRawCode()
    {
        return rawCode;
    }

    public string GetFuncName()
    {
        return funcName;
    }
}

/// <summary>
/// コースの設計を統括するデザイナークラスの抽象クラスです。
/// 設計範囲はクラスにより様々で、1つのコースパーツしか置かないクラスもあれば、
/// BranchCourseDesignerのように「branch_start」から「branch_end」まで挟まれた区間の
/// 設計を行うクラスもあります。
/// 
/// これらのクラスでは、Designメソッドを呼び出すことで、設計すべきコースパーツを反復的に
/// 配置し、次のコースパーツを置く場所を返すことが保証されています。
/// 
/// また、設計範囲を表すGetCourseWidth関数が定義されています。1つのコースパーツなら、1を返し、
/// LoopCourseDesignerなら、LoopCourseDesigner内のコース範囲 + 2を返します。
/// 
/// </summary>
public abstract class AbstractCourseDesigner
{
    public bool hasFunc;
    /// <summary>
    /// コースを配置する手続きです。呼び出されるたびにオブジェクトが
    /// 配置されていきます。
    /// </summary>
    /// <param name="putPos">一番最初の配置場所</param>
    /// <returns>次の配置場所</returns>
    public abstract IEnumerable<Transform> Design(Transform putPos);

    public abstract int GetCourseWidth();

    /// <summary>
    /// コースパーツを生成し、Endという名前のオブジェクトの座標系を返します。
    /// </summary>
    /// <param name="prefab"></param>
    /// <param name="putPos"></param>
    /// <returns></returns>
    /// 
    protected void CreateFuncGate(GameObject prefab, Transform putPos)
    {
        int id = GameObject.Find("RailBuilder").GetComponent<RailBuilder>().funcId;
        GameObject part = Object.Instantiate(prefab, putPos.position, putPos.rotation);
        part.transform.parent = putPos.parent.parent;
        //Debug.Log(part);
        //Debug.Log(railType);
        //part.GetComponent<FuncGate>().funcName = fname;
        //part.GetComponent<FuncGate>().GateID = id;
        FuncGate gate = part.transform.GetChild(0).GetComponentInChildren<FuncGate>();
        gate.GateID = id;
        //Debug.Log(gate);
        GameObject.Find("RailBuilder").GetComponent<RailBuilder>().funcId++;
    }
    protected Transform CreateCourse(GameObject prefab, Transform putPos, RailType railType)
    {
        
        //パーツ設置。　位置、角度決めも同時に行う。
        // modifired:ここでRailIDも同時に決定する。
        GameObject part = Object.Instantiate(prefab, putPos.position, putPos.rotation);
        //Debug.Log(part);
        //Debug.Log(railType);
        part.GetComponent<RailBase>().type = railType;
        //Debug.Log((int)part.GetComponent<RailBase>().type);
        if (part.GetComponent<RailBase>()!=null && part.GetComponent<RailBase>().type!=(RailType)0 && part.GetComponent<RailBase>().type != (RailType)4)
        {
            //Debug.Log("pre:"+GameObject.Find("RailBuilder").GetComponent<RailBuilder>().railId);
            part.GetComponent<RailBase>().railId = GameObject.Find("RailBuilder").GetComponent<RailBuilder>().railId;
            GameObject.Find("RailBuilder").GetComponent<RailBuilder>().railId++;
            //Debug.Log("post:"+GameObject.Find("RailBuilder").GetComponent<RailBuilder>().railId);
        }

        //設置したパーツの子要素から"End"と名のつくものを探して、次の設置位置を返す
        return part.transform.Find("End").transform;
    }
    protected Transform CreateCourse(GameObject prefab, Transform putPos, RailType railType, Transform startPoint)
    {
        //パーツ設置。　位置、角度決めも同時に行う。
        // modifired:ここでRailIDも同時に決定する。
        GameObject part = Object.Instantiate(prefab, putPos.position, putPos.rotation);
        //Debug.Log(part);
        //Debug.Log(railType);
        part.GetComponent<RailBase>().type = railType;
        //Debug.Log((int)part.GetComponent<RailBase>().type);
        if (part.GetComponent<RailBase>() != null && part.GetComponent<RailBase>().type != (RailType)0　&& part.GetComponent<RailBase>().type != (RailType)4)
        {
            Debug.Log("pre:" + GameObject.Find("RailBuilder").GetComponent<RailBuilder>().railId);
            part.GetComponent<RailBase>().railId = GameObject.Find("RailBuilder").GetComponent<RailBuilder>().railId;
            GameObject.Find("RailBuilder").GetComponent<RailBuilder>().railId++;
            Debug.Log("post:" + GameObject.Find("RailBuilder").GetComponent<RailBuilder>().railId);
        }
        part.GetComponent<RailBase>().startPoint = startPoint;
        //設置したパーツの子要素から"End"と名のつくものを探して、次の設置位置を返す
        return part.transform.Find("End").transform;
    }
}

/// <summary>
/// コースパーツを一つのみ配置するデザイナークラスです。
/// </summary>
public class BasicCourseDesigner : AbstractCourseDesigner
{
    private GameObject coursePartObj;

public BasicCourseDesigner(GameObject partObject,bool func)
    {
        coursePartObj = partObject;
        hasFunc = func;
    }

    public override IEnumerable<Transform> Design(Transform putPos)
    {
        if (hasFunc)
        {
            CreateFuncGate(GameObject.Find("RailBuilder").GetComponent<RailBuilder>().funcGate, putPos);
        }
        yield return CreateCourse(coursePartObj, putPos, 0);
    }

    public override int GetCourseWidth()
    {
        return 1;
    }
}

/// <summary>
/// if文のコースを設計するデザイナークラスです。
/// </summary>
public class BranchCourseDesigner : AbstractCourseDesigner
{
    private GameObject branchStart;
    private GameObject branchEnd;
    private GameObject branchConnect;
    private GameObject thenStart;
    private GameObject thenEnd;
    private GameObject brankRail;
    private List<AbstractCourseDesigner> thenCourseDesigners;
    private List<AbstractCourseDesigner> elseCourseDesigners;
    private int connectCount;
    private int thenWidth, elseWidth;

    public BranchCourseDesigner(GameObject brStart, GameObject brEnd, GameObject brConnect, GameObject brThenStart,
        GameObject brThenEnd, GameObject brank, List<AbstractCourseDesigner> thenList, List<AbstractCourseDesigner> elseList, int connects, bool func)
    {
        branchStart = brStart;
        branchEnd = brEnd;
        branchConnect = brConnect;
        thenStart = brThenStart;
        thenEnd = brThenEnd;
        brankRail = brank;
        thenCourseDesigners = thenList;
        elseCourseDesigners = elseList;
        connectCount = connects;
        thenWidth = thenList.Sum(entry => entry.GetCourseWidth()) + connectCount * 2;
        elseWidth = elseList.Sum(entry => entry.GetCourseWidth());
        hasFunc = func;
    }

    public override IEnumerable<Transform> Design(Transform putPos)
    {
        Transform nextPos;
        if (hasFunc)
        {
            CreateFuncGate(GameObject.Find("RailBuilder").GetComponent<RailBuilder>().funcGate, putPos);
        }
        // 分岐パーツを配置
        GameObject ifStartPart = Object.Instantiate(branchStart, putPos.position, putPos.rotation);
        ifStartPart.GetComponent<RailBase>().type =(RailType)1;
        //Debug.Log(ifStartPart);
        //Debug.Log(ifStartPart.GetComponent<RailBase>().type);
        if (ifStartPart.GetComponent<RailBase>() != null && ifStartPart.GetComponent<RailBase>().type != (RailType)0 )
        {
            //Debug.Log("pre:" + GameObject.Find("RailBuilder").GetComponent<RailBuilder>().railId);
            ifStartPart.GetComponent<RailBase>().railId = GameObject.Find("RailBuilder").GetComponent<RailBuilder>().railId;
            GameObject.Find("RailBuilder").GetComponent<RailBuilder>().railId++;
            //Debug.Log("post:" + GameObject.Find("RailBuilder").GetComponent<RailBuilder>().railId);
        }
        nextPos = ifStartPart.transform.Find("End");
        
        yield return nextPos;

        // Then側から配置していくので、中継ぎパーツを配置
        for (int i = 0; i < connectCount; ++i)
        {
            nextPos = CreateCourse(branchConnect, nextPos,0);
            yield return nextPos;
        }

        // Then側の開始パーツを配置
        nextPos = CreateCourse(thenStart, nextPos,0);
        yield return nextPos;

        // Then側のコースを設計する
        foreach (var design1 in thenCourseDesigners)
        {
            foreach (var nextPos1 in design1.Design(nextPos))
            {
                nextPos = nextPos1;
                yield return nextPos1;
            }
        }

        // Else側と比較し、ブランクパーツを配置する必要があるなら配置
        for (int j = 0; j < elseWidth - thenWidth; ++j)
        {
            nextPos = CreateCourse(brankRail, nextPos,0);
            yield return nextPos;
        }

        // Then側の終了パーツを配置
        nextPos = CreateCourse(thenEnd, nextPos,0);
        yield return nextPos;

        // Else側に戻ろうとする中継ぎパーツを配置
        for (int i = 0; i < connectCount; ++i)
        {
            nextPos = CreateCourse(branchConnect, nextPos,0);
            yield return nextPos;
        }

        // 設計位置をElse側の開始位置に設定
        nextPos = ifStartPart.transform.Find("ElseSideEnd");

        // Else側のコースを設計する
        foreach (var design2 in elseCourseDesigners)
        {
            foreach (var nextPos2 in design2.Design(nextPos))
            {
                nextPos = nextPos2;
                yield return nextPos2;
            }
        }

        // Then側と比較し、ブランクパーツを配置する必要があるなら配置
        for (int k = 0; k < thenWidth - elseWidth; ++k)
        {
            nextPos = CreateCourse(brankRail, nextPos,0);
            yield return nextPos;
        }

        // 分岐終了パーツを配置
        nextPos = CreateCourse(branchEnd, nextPos,0);
        yield return nextPos;
    }

    public override int GetCourseWidth()
    {
        return Mathf.Max(thenWidth, elseWidth) + 2;
    }
}

public class LoopCourseDesigner : AbstractCourseDesigner
{
    private GameObject loopStart, loopEnd, loopBypass, loopElev;
    private List<AbstractCourseDesigner> childCourseDesigners;
    private int courseWidth,blockId;

    public LoopCourseDesigner(GameObject lpStart, GameObject lpEnd, GameObject lpBypass, GameObject lpElev, List<AbstractCourseDesigner> courseDesigners, bool func)
    {
        loopStart = lpStart;
        loopEnd = lpEnd;
        loopBypass = lpBypass;
        loopElev = lpElev;
        childCourseDesigners = courseDesigners;
        courseWidth = childCourseDesigners.Sum(entry => entry.GetCourseWidth());
        hasFunc = func;
    }
    public override IEnumerable<Transform> Design(Transform putPos)
    {
        bool hasFuncInEnd=false;
        if (hasFunc)
        {
            if (loopStart.name == "ALoopStartVer1.2")
            {
                hasFuncInEnd = true;
            }
            else
            {
                CreateFuncGate(GameObject.Find("RailBuilder").GetComponent<RailBuilder>().funcGate, putPos);
            }
        }

        Transform nextPos = CreateCourse(loopStart, putPos,(RailType)3);
        blockId = nextPos.parent.GetComponent<RailBase>().railId;
        GameObject.Find("RailBuilder").GetComponent<RailBuilder>().blockStartPoint = putPos;
        if (nextPos.parent.Find("LaunchTarget"))//ターゲットがあるということは始点
        {
        }
        if (nextPos.parent.Find("BypassArc"))
        {
            nextPos.parent.Find("BypassArc").localScale = new Vector3(1, ((courseWidth) * 1f) + 2f, ((courseWidth) * 1f) + 1.9f);
            nextPos.parent.Find("BypassArc").position = new Vector3(nextPos.parent.Find("BypassArc").position.x, nextPos.parent.Find("BypassArc").position.y - courseWidth * 0.1f, nextPos.parent.Find("BypassArc").position.z+ courseWidth*0.02f);

        }

        yield return nextPos;

        foreach (var design in childCourseDesigners)
        {
            foreach (var nextPos1 in design.Design(nextPos))
            {
                nextPos = nextPos1;
                yield return nextPos1;
            }
        }
        //Debug.Log(putPos.position);

        nextPos = CreateCourse(loopEnd, nextPos,(RailType)0,putPos);
        nextPos.parent.GetComponent<RailBase>().railId=blockId;

        if (hasFuncInEnd)
        {
            CreateFuncGate(GameObject.Find("RailBuilder").GetComponent<RailBuilder>().funcGate, nextPos.parent);
        }

        if (nextPos.parent.Find("Launcher")!=null)//ジャンプ台があるということは終点
        {
            Vector3 targetPos = putPos.position+new Vector3(0, 1-1f, 3-3f);
            nextPos.parent.GetComponent<RailBase>().startPoint = GameObject.Find("RailBuilder").GetComponent<RailBuilder>().blockStartPoint;
            Object.Instantiate(GameObject.Find("RailBuilder").GetComponent<RailBuilder>().marker, 
                targetPos, 
                Quaternion.identity);
            nextPos.parent.Find("Launcher").GetComponent<LaunchBall>().targetPos = targetPos;
        }
        yield return nextPos;
    }

    public override int GetCourseWidth()
    {
        return courseWidth + 2;
    }
}

public class FuncCourseDesigner : AbstractCourseDesigner
{
    private GameObject startObject,returnObject;
    private List<AbstractCourseDesigner> childCourseDesigners;
    private int courseWidth;
    string funcName;
    bool hasFunc;

    public FuncCourseDesigner(GameObject funcStart, GameObject funcEnd, List<AbstractCourseDesigner> courseDesigners, string name,bool func)
    {
        startObject = funcStart;
        returnObject = funcEnd;
        childCourseDesigners = courseDesigners;
        courseWidth = childCourseDesigners.Sum(entry => entry.GetCourseWidth());
        funcName = name;
        hasFunc = func;
    }
    public override IEnumerable<Transform> Design(Transform putPos)
    {
        Transform nextPos = CreateCourse(startObject, putPos, (RailType)4);
        nextPos.gameObject.name = "Start";
        Transform Cource = GameObject.Instantiate(GameObject.Find("RailBuilder").GetComponent<RailBuilder>().courceBase, putPos.position, putPos.rotation).transform;
        nextPos.parent.transform.parent = Cource;
        Cource.name = funcName;
        yield return nextPos;

        foreach (var design in childCourseDesigners)
        {
            foreach (var nextPos1 in design.Design(nextPos))
            {
                nextPos = nextPos1;
                nextPos1.parent.parent = Cource;
                yield return nextPos1;
            }
        }
        //ebug.Log(putPos.position);
        //nextPos = CreateCourse(returnObject, nextPos, (RailType)4, putPos);
        //if (hasFunc)
        //{
        //    CreateFuncGate(GameObject.Find("RailBuilder").GetComponent<RailBuilder>().funcGate, nextPos.parent);
        //}
        //nextPos.parent.transform.parent = Cource.transform;
        Cource.GetComponent<TrackMaker>().MakeTrack();
        Cource.GetComponent<TrackMaker>().courceLength=courseWidth;
        //Debug.Log(nextPos);
        nextPos.position += new Vector3(20,0,0);
        yield return nextPos;
    }

    public override int GetCourseWidth()
    {
        return courseWidth + 2;
    }
}


public class RailBuilder : MonoBehaviour
{

    [SerializeField, Tooltip("読み込むファイル名")]
    public string fileName;
    [SerializeField, Tooltip("読み込む拡張子")]
    public string fileExtension;
    [SerializeField, Tooltip("再生速度")]
    private float sleepTime;
    [SerializeField]
    private GameObject
        animPlayer,
        basic, // 通常レール
        brank, // 空白レール

        brBase, // 分岐スタート
        brConnect, // 分岐Then側中継ぎ
        brThenStart, // 分岐Then側始め
        brThenEnd, // 分岐Then側終わり
        brEnd, // 分岐エンド

        alpStart, // 後判定ループスタート
        alpEnd, // 後判定ループエンド
        alpBypass, // 後判定ループ
        alpElev, // 後判定ループ

        blpStart, // 前判定ループスタート
        blpEnd, // 前判定ループエンド
        blpBypass, // 前判定ループ
        blpElev, // 前判定ループ

        funcStart,
        funcEnd,
        mainFuncEnd;

    public int railId;
    public Transform blockStartPoint;
    bool isOtherPrev;
    string currentFunc;
    public GameObject marker, funcGate, courceBase;
    public int funcId;

    public IEnumerator Start()
    {
        //Debug.Log(name+"start");
        // ファイルパスを取得
        string filePath = Application.dataPath + @"/" + fileName + "." + fileExtension;
        // ファイルパスからファイルを読み取り、行の配列を取得
        string[] lines = File.ReadAllLines(filePath);
       // 各行の属性を格納するリストを作成
        List<FileLineEntry> fileLineDatas = new List<FileLineEntry>();

        foreach (var line in lines)
        {
            // カンマ区切りで、最大分割数を3に設定する(ソースコード内の「,」で区切られる可能性があるため)
            string[] splits = line.Split(",".ToCharArray(), 4);
            // 0番目... 種類名, 1番目... ネスト数, 2番目... 関数の種類
            //foreach(string st in splits) Debug.LogWarning(st);
            if (splits.Length == 0)
            {
                continue;
            }
            FileLineEntry fileLineData = new FileLineEntry(splits[0], int.Parse(splits[1]), splits[2], splits[3]);
            //FileLineEntry fileLineData = new FileLineEntry(splits[0], int.Parse(splits[1]), splits[2], bool.Parse(splits[3]));
            fileLineDatas.Add(fileLineData);
        }

        // コース設計を行うデザイナークラスのリストを作成する
        List<AbstractCourseDesigner> courseDesigners = makeCourceDesign(fileLineDatas.GetEnumerator());
        // コースパーツの制作位置(初期位置はアタッチしているオブジェクト自身)
        Transform currentTransform = transform;

        foreach (var designer in courseDesigners)
        {
            // デザインメソッドを呼び出し、コースを配置した後時間を止める
            foreach (var nextPos in designer.Design(currentTransform))
            {
                currentTransform = nextPos;

                //yield return new WaitForSeconds(sleepTime);
            }
        }
        yield return null;
        animPlayer.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {

        
    }

    private List<AbstractCourseDesigner> makeCourceDesign(List<FileLineEntry>.Enumerator datas)
    {
        List<AbstractCourseDesigner> results = new List<AbstractCourseDesigner>();

        while (datas.MoveNext())
        {
            var entry = datas.Current;
            int startNest = entry.GetNest();

            switch (entry.GetTypeName())
            {
                case "other":
                    if (!isOtherPrev)
                    {
                        results.Add(new BasicCourseDesigner(basic,entry.GetRawCode()=="user_func"));
                    }

                    break;

                case "branch_start":

                    List<FileLineEntry> ifEntries = new List<FileLineEntry>(), elseEntries = new List<FileLineEntry>();

                    while (datas.MoveNext())
                    {
                        var entry1 = datas.Current;
                        
                        if (entry1.GetTypeName() == "branch_else" && entry1.GetNest() == startNest)
                        {
                            break;
                        }
                        else
                        {
                            ifEntries.Add(entry1);
                        }
                    }

                    int connectCount = 0;

                    while (datas.MoveNext())
                    {
                        var entry2 = datas.Current;

                        if (entry2.GetTypeName() == "branch_end" && entry2.GetNest() == startNest)
                        {
                            break;
                        }
                        else
                        {
                            int tempCount = entry2.GetNest() - startNest;

                            connectCount = Mathf.Max(connectCount, tempCount);
                            elseEntries.Add(entry2);
                        }
                    }

                    var thenDesigners = makeCourceDesign(ifEntries.GetEnumerator());
                    var elseDesigners = makeCourceDesign(elseEntries.GetEnumerator());

                    results.Add(new BranchCourseDesigner(brBase, brEnd, brConnect, brThenStart, brThenEnd, brank, thenDesigners, elseDesigners, connectCount, entry.GetRawCode() == "user_func"));
                    break;

                case "aloop_start":

                    List<FileLineEntry> entries1 = new List<FileLineEntry>();

                    while (datas.MoveNext())
                    {
                        var entry1 = datas.Current;

                        if (entry1.GetTypeName() == "aloop_end" && entry1.GetNest() == startNest)
                        {
                            break;
                        }
                        else
                        {
                            entries1.Add(entry1);
                        }
                    }

                    results.Add(new LoopCourseDesigner(alpStart, alpEnd, alpBypass, alpElev, makeCourceDesign(entries1.GetEnumerator()), entry.GetRawCode() == "user_func"));
                    break;

                case "bloop_start":

                    List<FileLineEntry> entries2 = new List<FileLineEntry>();
                    while (datas.MoveNext())
                    {
                        var entry1 = datas.Current;

                        if (entry1.GetTypeName() == "bloop_end" && entry1.GetNest() == startNest)
                        {
                            break;
                        }
                        else
                        {
                            entries2.Add(entry1);
                        }
                    }

                    results.Add(new LoopCourseDesigner(blpStart, blpEnd, blpBypass, blpElev, makeCourceDesign(entries2.GetEnumerator()), entry.GetRawCode() == "user_func"));
                    break;

                case "func_start":
                    currentFunc = entry.GetRawCode();
                    bool hasFuncInEnd=false;
                    List<FileLineEntry> entries3 = new List<FileLineEntry>();
                    while (datas.MoveNext())
                    {
                        var entry1 = datas.Current;

                        if (entry1.GetTypeName() == "func_end" && entry1.GetNest() == startNest)
                        {
                            if (entry1.GetRawCode() == "user_func") hasFuncInEnd = true;
                            break;
                        }
                        else
                        {
                            entries3.Add(entry1);
                        }
                    }
                    if (entry.GetFuncName()=="main")
                    {
                        results.Add(new FuncCourseDesigner(funcStart, mainFuncEnd, makeCourceDesign(entries3.GetEnumerator()), entry.GetFuncName(), hasFuncInEnd));
                    }
                    else results.Add(new FuncCourseDesigner(funcStart,funcEnd, makeCourceDesign(entries3.GetEnumerator()),entry.GetFuncName(), hasFuncInEnd));
                    break;

                case "return":
                    //List<FileLineEntry>.Enumerator entry4 = datas;
                    //entry4.MoveNext();
                    //FileLineEntry entry5 = entry4.Current;
                    //if (entry5.GetTypeName() != "func_end")
                    {
                        if (entry.GetFuncName() == "main")
                        {
                            results.Add(new BasicCourseDesigner(mainFuncEnd, entry.GetRawCode() == "user_func"));
                        }
                        else results.Add(new BasicCourseDesigner(funcEnd, entry.GetRawCode() == "user_func"));
                    }

                    break;
            }

        }

        return results;
    }

}
