﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SpeedManager : MonoBehaviour
{
    public int maxSpeed;
    private int defaultSpeed = 4, prevSpeed;
    int currentSpeed;
    [SerializeField]
    public float speedMp = 1;
    public float stopTime = 1f;
    public TextMeshProUGUI text;
    public bool isTimeStopping=false, isAbleTimeStop = true;
    //いずれはボール側のステートで巻き戻し可否を判断したい
    // Start is called before the first frame update
    void Start()
    {
        currentSpeed = defaultSpeed;
        UpdateTimeText();
        prevSpeed = currentSpeed;
        Time.timeScale = currentSpeed*speedMp;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("up"))
        {
            if (currentSpeed < maxSpeed*defaultSpeed)
            {
                currentSpeed = currentSpeed * 2;
            }
            if (currentSpeed == 0)
            {
                currentSpeed = 1;
            }

            UpdateTimeText();
        }

        if (Input.GetKeyDown("down"))
        {
            if (currentSpeed > 1)
            {
                currentSpeed = currentSpeed / 2;
            }

            UpdateTimeText();
        }

        if (Input.GetKeyDown(KeyCode.Space) && isAbleTimeStop)
        {
            if (Time.timeScale != 0f)
            {
                Debug.Log("Time Stop");
                SetTimeStop(true);
            }
            else
            {
                Debug.Log("Time Start");
                SetTimeStop(false);
            }
        }
        
        Time.timeScale = speedMp*currentSpeed / 4f;
    }

    public void UpdateTimeText()
    {
        text.text = "Current speed : " + (float)currentSpeed / defaultSpeed + "x";
    }

    public void SetTimeStop(bool _bool)
    {
        if (isAbleTimeStop)
            if (_bool)
            {
                if (prevSpeed > 0) prevSpeed = currentSpeed;
                currentSpeed = 0;
            }
            else
            {
                currentSpeed = prevSpeed;
            }
        else Debug.LogError("今は時間を止められない");
        UpdateTimeText();

    }
}
