# include <stdio.h>
# define NUM 100
int main ( void )
{
    int ___RETURN_VALUE_TEMP;
    printf ( "\nfunc_start,main\n" ) ;
    int i ;
    for ( i = 1 ; i <= NUM ; i++ ) {
        printf ( "\nbloop_start,0\n" ) ;
        if ( i % 3 == 0 ) {
            printf ( "\nthen,1\n" ) ;
            printf ( "%d:fizz" , i ) ;
            if ( i % 5 == 0 ) {
                printf ( "\nthen,2\n" ) ;
                printf ( "buzz" ) ;
            } else {
                printf ( "\nelse,2\n" ) ;
            }
        } else {
            printf ( "\nelse,1\n" ) ;
            if ( i % 5 == 0 ) {
                printf ( "\nthen,3\n" ) ;
                printf ( "%d:buzz" , i ) ;
            } else {
                printf ( "\nelse,3\n" ) ;
                printf ( "%d" , i ) ;
            }
            printf ( "\n" ) ;
        }
    }
    printf ( "\nbloop_bottom,0\n" ) ;
    ___RETURN_VALUE_TEMP = ( 0 ) ;
    printf ( "\nfunc_end,main\n" ) ;
    return ___RETURN_VALUE_TEMP ;
}
